import React, { PureComponent, Component } from "react"
import styled from "styled-components"
import { Droppable } from "react-beautiful-dnd"
import { Task } from "./Task"

const Container = styled.div`
  margin: 8px;
  border: 1px solid lightgrey;
  border-radius: 2px;
  width: 220px;
  display: flex;
  flex-direction: column;
`

const Title = styled.h3`
  padding: 8px;
`
const Tasklist = styled.div`
  padding: 8px;
  transition: background-color 1s ease;
  background-color: ${props => (props.isDraggingOver ? "skyblue" : "white")};
  flex-grow: 1;
  min-height: 100px;
`

class InnerList extends PureComponent {
  render() {
    return this.props.tasks.map((task, index) => (
      <Task task={task} key={task.id} index={index} />
    ))
  }
}

class Column extends Component {
  render() {
    return (
      <Container>
        <Title>{this.props.column.title}</Title>
        <Droppable droppableId={this.props.column.id}>
          {(provided, snapshot) => (
            <Tasklist
              {...provided.droppableProps}
              innerRef={provided.innerRef}
              isDraggingOver={snapshot.isDraggingOver}
            >
              <InnerList tasks={this.props.tasks} />
              {provided.placeholder}
            </Tasklist>
          )}
        </Droppable>
      </Container>
    )
  }
}

export default Column
