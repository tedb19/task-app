import React, { Component } from "react"
import ReactDOM from "react-dom"
import Column from "./Column"
import "@atlaskit/css-reset"
import { DragDropContext } from "react-beautiful-dnd"
import { initialData } from "./initialData"
import styled from "styled-components"

const Container = styled.div`
  display: flex;
`

class App extends Component {
  state = initialData

  /**
   * The result param has the following shape:
   *
   *  {
   *    draggableId: 'task-1',
   *    type: 'TYPE',
   *    reason: 'DROP',
   *    source: {
   *      droppableId: 'column-1',
   *      index: 0
   *    },
   *    destination: {
   *      droppableId: 'column-1',
   *      index: 1
   *    },
   *  }
   */
  onDragEnd = result => {
    const { destination, source } = result

    if (!destination) return

    if (
      destination.droppableId === source.droppableId &&
      destination.index === source.index
    )
      return

    const srcColumn = this.state.columns[source.droppableId]
    const destColumn = this.state.columns[destination.droppableId]

    if (srcColumn.id === destColumn.id) {
      const srcTaskIds = srcColumn.taskIds

      const newColumn = {
        ...srcColumn,
        taskIds: this.reorder(srcTaskIds, source.index, destination.index)
      }

      const newState = {
        ...this.state,
        columns: {
          ...this.state.columns,
          [newColumn.id]: newColumn
        }
      }

      this.setState(newState)
      return
    }

    const srcTaskIds = Array.from(srcColumn.taskIds)
    const destTaskIds = Array.from(destColumn.taskIds)

    const newTasksIds = this.reorderOnDifferentLists(
      srcTaskIds,
      destTaskIds,
      source.index,
      destination.index
    )

    const newSrcColumn = {
      ...srcColumn,
      taskIds: newTasksIds.srcResult
    }

    const newDestColumn = {
      ...destColumn,
      taskIds: newTasksIds.destResult
    }

    const newState = {
      ...this.state,
      columns: {
        ...this.state.columns,
        [newSrcColumn.id]: newSrcColumn,
        [newDestColumn.id]: newDestColumn
      }
    }

    this.setState(newState)
  }

  reorder = (items, sourceIndex, destIndex) => {
    // create a new, shallow-copied Array instance from items
    const result = Array.from(items)
    const [removed] = result.splice(sourceIndex, 1)
    result.splice(destIndex, 0, removed)
    return result
  }

  reorderOnDifferentLists = (sourceList, destList, sourceIndex, destIndex) => {
    const srcResult = Array.from(sourceList)
    const destResult = Array.from(destList)
    const [removed] = srcResult.splice(sourceIndex, 1)
    destResult.splice(destIndex, 0, removed)
    return { srcResult, destResult }
  }

  render() {
    return (
      <DragDropContext onDragEnd={this.onDragEnd}>
        <Container>
          {this.state.columnOrder.map(columnId => {
            const column = this.state.columns[columnId]
            const tasks = column.taskIds.map(taskId => this.state.tasks[taskId])

            return <Column key={column.id} column={column} tasks={tasks} />
          })}
        </Container>
      </DragDropContext>
    )
  }
}

ReactDOM.render(<App />, document.getElementById("root"))
