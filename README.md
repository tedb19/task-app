## Task App

A simple react app to demonstrate the following:

- `react-beautiful-dnd`, which is a drag and drop library by atlassian. Its very robust and is whats recommended right now.

- `styled-components`, which is a component-based css-in-js solution.

- `atlassian css reset`, which comes with a few css resets.
